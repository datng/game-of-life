﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "dialog_about.h"

#include <QDebug>
#include <QDialog>
#include <QMessageBox>
#include <QTimer>

#include <algorithm>
#include <cmath>

const static QColor QCOLOR_BLACK(0, 0, 0);
const static QColor QCOLOR_WHITE(255, 255, 255);

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    setWindowTitle("Conway's Game of Life");
#ifdef _WIN32
    setWindowFlag(Qt::MSWindowsFixedSizeDialogHint); // disable resize grip (Windows only)
#endif
    // start the timer
    m_Timer = new QTimer(this);
    connect(m_Timer, &QTimer::timeout, this, &MainWindow::Tick);
    // 40ms ticks to achieve 25fps
    m_Timer->start(40);

    // seed randomization
    auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    srand(static_cast<unsigned int>(seed));
}

MainWindow::~MainWindow() {
    delete ui;
    delete m_Timer;
}

void MainWindow::Paint(QPainter *ePainter, QPaintEvent *event) {
    ePainter->fillRect(event->rect(), QBrush(QColor(255, 255, 255)));

    for (size_t row = 0; row < m_State.size(); ++row) {
        for (size_t column = 0; column < m_State.at(row).size(); ++column) {
            if (CellState::LIVE == m_State.at(row).at(column)) {
                ePainter->fillRect(
                    event->rect().x() + static_cast<int>(column * m_CellSize), //
                    event->rect().y() + static_cast<int>(row * m_CellSize),
                    static_cast<int>(m_CellSize),
                    static_cast<int>(m_CellSize),
                    QBrush(QCOLOR_BLACK));
            }
        }
    }
}

// is called upon every update()
void MainWindow::paintEvent(QPaintEvent *event) {
    QPainter painter;
    painter.begin(ui->openGLWidget);
    painter.setRenderHint(QPainter::Antialiasing);
    Paint(&painter, event);
    painter.end();
}

void MainWindow::ClearState() {
    for (auto &row : m_State) {
        for (size_t column = 0; column < row.size(); ++column) {
            row.at(column) = CellState::DEAD;
        }
    }
}

void MainWindow::RandomizeState() {
    for (auto &row : m_State) {
        for (size_t column = 0; column < row.size(); ++column) {
            auto temp = rand() % 2;
            if (temp) {
                row.at(column) = CellState::LIVE;
            } else {
                row.at(column) = CellState::DEAD;
            }
        }
    }
}

void MainWindow::StepSimulation() {
    std::vector<std::vector<CellState>> updatedStates(m_State);

    for (size_t row = 0; row < updatedStates.size(); ++row) {
        for (size_t column = 0; column < updatedStates.at(row).size(); ++column) {
            unsigned int neighborCount = 0;
            for (size_t i = std::max(static_cast<size_t>(0), row - 1); i <= std::min(updatedStates.size() - 1, row + 1); ++i) {
                for (size_t j = std::max(static_cast<size_t>(0), column - 1); j <= std::min(updatedStates.at(row).size() - 1, column + 1); ++j) {
                    if (CellState::LIVE == m_State.at(i).at(j) && (i != row || j != column)) { ++neighborCount; }
                }
            }

            // calculate next generation according to the rules
            // for live cells
            switch (m_State.at(row).at(column)) {
            case CellState::LIVE:
                switch (neighborCount) {
                // live cells with 2 or 3 neighbors live on
                case 2:
                case 3:
                    updatedStates.at(row).at(column) = CellState::LIVE;
                    break;
                // live cells with fewer than 2 neighbors die of underpopulation
                // and those with more than 3 neighbors die of overpopulation
                default:
                    updatedStates.at(row).at(column) = CellState::DEAD;
                    break;
                }
                break;
            case CellState::DEAD:
                switch (neighborCount) {
                // dead cells with exactly 3 neighbors become live
                case 3:
                    updatedStates.at(row).at(column) = CellState::LIVE;
                    break;
                default:
                    updatedStates.at(row).at(column) = CellState::DEAD;
                    break;
                }
            }
        }
    }
    m_State = updatedStates;
}

void MainWindow::Tick() {
    m_State.resize(static_cast<size_t>(ui->openGLWidget->height() / static_cast<int>(m_CellSize)));
    for (auto &row : m_State) {
        row.resize(static_cast<size_t>(ui->openGLWidget->width() / static_cast<int>(m_CellSize)));
    }
    if (m_Simulating) { StepSimulation(); }
    update();
}

void MainWindow::on_pushButton_StartSimulation_clicked() {
    m_Simulating = !m_Simulating;
    if (m_Simulating) {
        ui->pushButton_StartSimulation->setText("Stop Simulation");
    } else {
        ui->pushButton_StartSimulation->setText("Start Simulation");
    }
}

void MainWindow::on_pushButton_Randomize_clicked() {
    RandomizeState();
}

void MainWindow::on_pushButton_Clear_clicked() {
    ClearState();
}

void MainWindow::on_actionExit_triggered() {
    close();
}

void MainWindow::on_actionAbout_triggered() {
    if (m_Dialog_About) delete m_Dialog_About;
    m_Dialog_About = new Dialog_About(this);
    m_Dialog_About->setModal(true);
    m_Dialog_About->show();
}

void MainWindow::SetCellSize(const unsigned int &eExponent) {
    auto exponent = std::min<unsigned int>(eExponent, MAXIMUM_CELL_SIZE_EXPONENT);
    auto size = static_cast<unsigned int>(std::pow(2, exponent));
    m_CellSize = size;
    if (ui->horizontalSlider_CellSize->value() != static_cast<int>(exponent)) ui->horizontalSlider_CellSize->setValue(static_cast<int>(exponent));
    if (ui->lineEdit_CellSize->text() != QString::number(exponent)) ui->lineEdit_CellSize->setText(QString::number(exponent));
}

void MainWindow::on_horizontalSlider_CellSize_valueChanged(int value) {
    SetCellSize(static_cast<unsigned int>(value));
}

void MainWindow::on_lineEdit_CellSize_returnPressed() {
    unsigned int value = ui->lineEdit_CellSize->text().toUInt();
    SetCellSize(value);
}

void MainWindow::mouseMoveEvent(QMouseEvent *event) {
    if (m_MousePressed) {
        QPoint cellPos = GetCellPosition(event);
        if (cellPos != INVALID_CELL_POSITION && cellPos != m_LastClickedPosition && m_LastClickedPosition != INVALID_CELL_POSITION) {
            // trace a line from last position to current position
            std::vector<QPoint> affectedCells;
            affectedCells.push_back(cellPos);

            int lineDensity = std::max( //
                std::abs(cellPos.x() - m_LastClickedPosition.x()),
                std::abs(cellPos.y() - m_LastClickedPosition.y()) //
            );
            /* for some reason, the line from last clicked position to current cell position is not filled without a multiplier,
             * 4 seems to do the trick, though further investigation is needed */
            lineDensity *= 4;

            for (int i = 0; i < lineDensity; ++i) {
                QPoint temp = m_LastClickedPosition + (cellPos - m_LastClickedPosition) * i / lineDensity;
                if (temp != affectedCells.back()) affectedCells.push_back(temp);
            }
            for (auto &affectedCell : affectedCells) {
                SwitchState(affectedCell);
            }

            m_LastClickedPosition = cellPos;
        }
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event) {
    m_MousePressed = true;

    QPoint cellPos = GetCellPosition(event);
    if (cellPos != INVALID_CELL_POSITION) {
        m_LastClickedPosition = cellPos;
        SwitchState(cellPos);
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event) {
    if (event) m_MousePressed = false;
}

QPoint MainWindow::GetCellPosition(QMouseEvent *event) {
    // get mouse position relative to the openGLWidget showing the game
    auto relativeMousePosition = QPoint(event->x() - ui->openGLWidget->x(), event->y() - ui->openGLWidget->y() - menuBar()->height());
    // check if such position belongs inside the openGLWidget
    if ( //
        relativeMousePosition.x() < 0 //
        || relativeMousePosition.x() >= ui->openGLWidget->width() //
        || relativeMousePosition.y() < 0 //
        || relativeMousePosition.y() >= ui->openGLWidget->height())
        return QPoint(-1, -1);
    // transform that position to cell position in the state grid
    QPoint cellPos(relativeMousePosition.x() / static_cast<int>(m_CellSize), relativeMousePosition.y() / static_cast<int>(m_CellSize));
    return cellPos;
}

void MainWindow::SwitchState(QPoint eCellPosition) {
    CellState *cell = &m_State[static_cast<unsigned int>(eCellPosition.y())][static_cast<unsigned int>(eCellPosition.x())];
    switch (*cell) {
    case CellState::DEAD:
        *cell = CellState::LIVE;
        break;
    case CellState::LIVE:
        *cell = CellState::DEAD;
    }
}
