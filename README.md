# Conway's Game of Life
A Qt implementation of John Horton Conway's cellular automation.

## Dependencies
- A C++11 compiler
- Qt Creator and QT libraries: core, gui, widgets

## Compilation
- The [project file](game-of-life.pro) can be opened by Qt Creator for compilation.
- Alternatively, use qmake and make
  - On OpenSUSE:
    ```
    qmake-qt5
    make
    ```
