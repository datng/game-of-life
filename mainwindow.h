﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <dialog_about.h>

#include <QPaintEvent>
#include <QPainter>
#include <QTimer>

enum class CellState { DEAD, LIVE };
const QPoint INVALID_CELL_POSITION = QPoint(-1, -1);
const unsigned int MAXIMUM_CELL_SIZE_EXPONENT = 5;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);

private slots:
    void on_actionAbout_triggered();
    void on_actionExit_triggered();
    void on_horizontalSlider_CellSize_valueChanged(int value);
    void on_lineEdit_CellSize_returnPressed();
    void on_pushButton_Clear_clicked();
    void on_pushButton_Randomize_clicked();
    void on_pushButton_StartSimulation_clicked();
    void SetCellSize(const unsigned int &eSize);
    void Tick();

private:
    void ClearState();
    QPoint GetCellPosition(QMouseEvent *event);
    void Paint(QPainter *ePainter, QPaintEvent *event);
    void RandomizeState();
    void StepSimulation();
    void SwitchState(QPoint eCellPosition);

    unsigned int m_CellSize = 4;

    Dialog_About *m_Dialog_About = nullptr;

    QPoint m_LastClickedPosition = INVALID_CELL_POSITION;

    bool m_MousePressed = false;

    bool m_Simulating = false;

    std::vector<std::vector<CellState>> m_State;
    QTimer *m_Timer = nullptr;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
